// Import all pipes
import { EditableComboboxFilterPipe } from './pipes/editable-combobox-filter.pipe';


// Export all pipes
export * from './pipes/editable-combobox-filter.pipe';

// Export convenience property
export const PIPES: any[] = [
  EditableComboboxFilterPipe
];
