import { NgModule, } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { EditableComboboxComponent, ComboboxItem } from './editable-combobox.component';
import { ComboboxItemRendererComponent } from './combobox-item-renderer.component';
import { EditableComboboxFilterPipe } from './../../pipes/editable-combobox-filter.pipe';

@NgModule({
    imports: [FormsModule, BrowserModule],
    declarations: [EditableComboboxComponent, ComboboxItem, ComboboxItemRendererComponent, EditableComboboxFilterPipe],
    exports: [ComboboxItem, EditableComboboxComponent]
})
export class EditableComboboxModule { }

