import { ThisComboboxItemComponent, itemTypeEnum } from './this-combobox-item.component';
import { ComboboxItemRendererComponent } from './combobox-item-renderer.component';

import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Component, forwardRef, OnInit, AfterViewInit, AfterContentInit, Renderer, ElementRef, Attribute } from '@angular/core';
import { Input, Output, EventEmitter, ContentChildren, ViewChild, QueryList, ViewChildren } from '@angular/core';

const noop = () => { };

@Component({
  selector: 'editable-combobox',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => EditableComboboxComponent),
    multi: true
  }
  ],
  templateUrl: 'editable-combobox.component.html',
  styleUrls: ['editable-combobox.component.css']
})
export class EditableComboboxComponent implements OnInit, AfterViewInit, AfterContentInit, ControlValueAccessor {
  /*an string containing the placeholder for the input field */
  @Input() placeholder: string = '';
  /*an array containing all the dropdown elements */
  @Input() values: IComboboxItem[];
  /* setter for the position of the values defined by a enum */
  @Input() set valuesPosition(val: any) { this._valuePosition = val; }
  /* emit the data-property from the selected element in the dropdownlist */
  @Output() selectedItem: EventEmitter<any> = new EventEmitter();

  /* reference to all the inline defined ComboboxItem */
  @ContentChildren(forwardRef(() => ComboboxItem)) inlineValues: QueryList<any>;
  /* references to the textinput and dropdownlist component inside the component */
  @ViewChild('textInput') textInput: any;
  @ViewChild('dropdownList') dropdownList: any;
  /* reference to the filtered values of the dropdownlist when the list is showing */
  @ViewChildren(forwardRef(() => ComboboxItemRendererComponent)) filteredValues: QueryList<any>;


  private _valuePosition: valuesPositionEnum = valuesPositionEnum.sorted;
  private _searchValue: string = '';
  private _inited: Boolean = false;

  /* the array containing the combined values of the values property and the inlineValues */
  private allValues: ThisComboboxItemComponent[];

  /* control that holds the visibility of the dropdownlist */
  private showDropDown: boolean = false;
  /* control to emit a null selectedItem when changing the searchValue after an item was selected */
  private resetEmitted: boolean = true;
  /* contol the checks of an item was selected by subscribing to the selectedItem EventEmitter*/
  private itemSelected: boolean = false;

  ngOnInit() {
    this.selectedItem.subscribe((item: IComboboxItem) => { this.itemSelected = !(item == null); });
    this.onSelectEventHandler = this.onSelectEventHandler.bind(this);

  }

  /* grab the inline defined ComboboxItem */
  ngAfterContentInit() {
    if (!this.inlineValues) { return; }
    this.allValues = this.inlineValues.toArray();
    this.allValues.forEach((item: ThisComboboxItemComponent) => {
      item.type = itemTypeEnum.static;
    });
  }

  /* 
   * grab the ComboboxItems defined through the values property
   * and combine the array according to the valuesPosition 
  */
  ngAfterViewInit() {
    if (this.values) {
      this.values.forEach((item: ThisComboboxItemComponent) => {
        item.type = itemTypeEnum.dynamic;
      });
      this.allValues = this._valuePosition === valuesPositionEnum.below ?
        this.allValues.concat(<ThisComboboxItemComponent[]>this.values) : <ThisComboboxItemComponent[]>this.values.concat(this.allValues);
    }
    if (this._valuePosition === valuesPositionEnum.sorted) {
      this.allValues.sort((a: IComboboxItem, b: IComboboxItem) => {
        if (a.text < b.text) {
          return -1;
        }
        else {
          if (a.text > b.text) {
            return 1;
          }
        }
        return 0;
      });
    }
    this._inited = true;
  }

  ngOnChanges(changes: any): void {
    if (!this._inited) return;
    if ("placeholder" in changes) {
      this.placeholder = changes.placeholder.currentValue;
    }
    if ("valuesPosition" in changes) {
      this._valuePosition = changes.valuesPosition.currentValue;
    }
    if ("values" in changes) {
      this.values = changes.values.currentValue;
      this.ngAfterContentInit();
      this.ngAfterViewInit();

    }
  }

  private modelChangeEventHandler(event: any) {
    this.showDropDown = true;
    this.onResetEventHandler();
    this.onTouchedCallback();
    if (this.filteredValues !== undefined) {
      if (this.filteredValues.length === 1) {
        if (this.filteredValues.first.text.toLowerCase() === this._searchValue.toLowerCase()) {
          this.onSelectEventHandler(this.filteredValues.first.item);
        }
      }
    }
  }

  private clickEventHandler(event: any) {
    if (event.target.id === 'chevron') {
      this.showDropDown = !this.showDropDown;
    }
    if (event.target.id === 'clear') {
      this._searchValue = '';
      this.onResetEventHandler();
      this.onTouchedCallback();
      this.onChangeCallback(null);
    }
  }

  private keyEventHandler(event: any) {
    if (this._searchValue !== '') {
      return;
    }
    if (event.target === this.textInput.nativeElement) {
      switch (event.keyCode) {
        // arrow down
        case 40:
          if (!this.showDropDown) { this.showDropDown = true; }
          return;
        // arrow up  
        case 38:
        // esc
        case 27:
          if (this.showDropDown) { this.showDropDown = false; }
          return;
      }
    };
    switch (event.keyCode) {
      // esc
      case 27:
        this.showDropDown = false;
        return;
    }
  }

  private onSelectEventHandler(item: IComboboxItem, changeState: boolean = true) {
    this._searchValue = item.text;
    this.showDropDown = false;
    this.resetEmitted = false;
    this.selectedItem.emit(item);
    if (!changeState) {
      return;
    }
    this.onTouchedCallback();
    this.onChangeCallback(item.id);
  }

  private onResetEventHandler() {
    if (this.resetEmitted) {
      return;
    }
    this.resetEmitted = true;
    this.selectedItem.emit(null);
    this.onChangeCallback(null);
  }

  private onBlurEventHandler(event: any) {
    if (!this.itemSelected && this._searchValue !== '' && this.filteredValues.length === 0) {
      this._searchValue = '';
    }
  }

  /* ControlValueAccessor implementation */
  isPropagate: boolean = false;

  private _renderer: Renderer;
  private _elementRef: ElementRef;
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  private _disabled: boolean = false;

  constructor(_renderer: Renderer, _elementRef: ElementRef) {
    this._renderer = _renderer;
    this._elementRef = _elementRef;
  };

  registerOnChange(fn: any) {
    if (this.isPropagate) { return; }
    this.onChangeCallback = fn;
    this.isPropagate = true;
  }
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
  setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }
  writeValue(value: string) {
    if (value !== undefined && value !== null && this.allValues !== undefined) {
      this.allValues.forEach((item: ThisComboboxItemComponent) => {
        if (item.id === value) {
          this.onSelectEventHandler(item, false);
          return;
        }
      });
    }
  }
}

export interface IComboboxItem { id: string; text: string; };

@Component({
  selector: 'combobox-item',
  template: ''
})
export class ComboboxItem implements IComboboxItem {
  constructor( @Attribute('id') public id: string, @Attribute('text') public text: string) { }
}
export enum valuesPositionEnum { below, before, sorted }
