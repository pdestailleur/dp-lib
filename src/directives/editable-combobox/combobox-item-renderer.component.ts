import { ThisComboboxItemComponent } from './this-combobox-item.component';
import { Component, Input } from '@angular/core';
/* 
 * component to display the elements of the dropdownlist
 * handles the click and emits a select event throught the service
 */
@Component({
  selector: 'combobox-item-renderer',
  templateUrl: 'combobox-item-renderer.component.html',
  styleUrls: ['combobox-item-renderer.component.css']
})
export class ComboboxItemRendererComponent {
  @Input() item: ThisComboboxItemComponent;
  /* the function callback executed when clicked */
  @Input() click: Function;

  private clickEventHandler(event: Event) {
    this.click(this.item);
    event.preventDefault();
  }
  /* 
    * helper function to get the class based on item type
    * more flexible than defining all the type with a class. - selector
  */
  get class(): string {
    return 'dropdown-item ' + this.item.type;
  }
  get text(): string {
    return this.item.text;
  }
}
