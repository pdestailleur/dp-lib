import { IComboboxItem } from '.';
import { Component } from '@angular/core';

export enum itemTypeEnum { static, dynamic };

@Component({
	selector: 'this-combobox-item-component-notrendered',
	template: ''
})
export class ThisComboboxItemComponent implements IComboboxItem {
	private _type: itemTypeEnum;

	constructor(public id: string, public text: string) {}

	get type(): itemTypeEnum {
		return this._type;
	}

	set type(val: itemTypeEnum) {
		if (this._type === undefined) {
			this._type = val;
		}	
	}
}
