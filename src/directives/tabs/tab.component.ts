import { Input, Component } from '@angular/core';

@Component({
  selector: 'tab',
  templateUrl: 'tab.component.html'
})
export class Tab {

  @Input() title: string;
  @Input() selected = false;
}
