import { Tab } from './tab.component';
import { Component, ContentChildren, QueryList, AfterContentInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'tabs',
  templateUrl: 'tabs.component.html'
})
export class Tabs implements AfterContentInit {
  @ContentChildren(Tab) tabs: QueryList<Tab>;
  @Output() selected = new EventEmitter();

	ngAfterContentInit() {
		let activeTabs = this.tabs.filter((tab) => tab.selected);
		if (activeTabs.length === 0) {
		this.selectTab(this.tabs.first);
		}
	}

	selectTab(tab: Tab) {
		this.tabs.toArray().forEach(tab => tab.selected = false);
		tab.selected = true;
		this.selected.emit({selectedTab: tab});
	}
}
