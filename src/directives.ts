// Import all directives
import {EditableComboboxModule} from './directives/editable-combobox';


// Export all directives
export * from './directives/editable-combobox';
export * from './directives/tabs';


// Export convenience property
export const DIRECTIVES: any[] = [
  EditableComboboxModule
];
