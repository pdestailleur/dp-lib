import { ComboboxItem } from './../directives/editable-combobox';
import {Pipe, PipeTransform } from '@angular/core';

/* Pipe to show only the matching elements in the dropdownlist */
@Pipe({
  name: 'EditableComboboxFilterPipe'
})
export class EditableComboboxFilterPipe implements PipeTransform {
  transform(array: ComboboxItem[], searchValue: string): ComboboxItem[] {
    return searchValue ? array.filter(item => item.text.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0) : array;
  }
}
