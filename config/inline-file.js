const fs = require('fs');
const path = require('path');
const glob = require('glob');
const root = require('./helpers').root;

const SRC_DIR = 'src';
const DIST_DIR = 'dist';
const stylesBlockRegExp = /styleUrls*:\s*(\[[^\]]+])/i;
const templateBlockRegExp = /templateUrl\s*:\s*('.*')/i;

function getJsFiles() {
    return glob.sync('**/*.js', { cwd: root(DIST_DIR), dot: true })
        .map(file => { return { path: path.dirname(file), name: path.basename(file) } });
}

function readFile(fullFileName) {
    return fs.readFileSync(fullFileName, { encoding: 'UTF-8' });
}

function writeFile(fullFileName, content) {
    fs.writeFileSync(fullFileName, content, { flag: 'w' });
}

function inlineStyles(file) {
    const fullFileName = root(DIST_DIR + '/' + file.path + '/' + file.name);
    const fileString = readFile(fullFileName);
    let result = fileString;

    while (styleUrls = result.match(stylesBlockRegExp)) {
        const filename = styleUrls[1].replace(/\[/g, '').replace(/\]/g, '').replace(/\'/g, '');
        const cssString = readFile(root(SRC_DIR + '/' + file.path + '/' + filename));
        let strippedString = cssString.split('\n').map(function(line) { return line.trim(); }).join('').split('\b').map(function(line) { return line.trim(); }).join('');
        result = result.replace(styleUrls[0], 'styles: [`' + strippedString + '`]');
    }

    if (result !== fileString) {
        writeFile(fullFileName, result);
    }
}

function inlineHtml(file) {
    const fullFileName = root(DIST_DIR + '/' + file.path + '/' + file.name);
    const fileString = readFile(fullFileName);
    let result = fileString;

    while (templateUrl = result.match(templateBlockRegExp)) {
        const filename = templateUrl[1].replace(/\[/g, '').replace(/\]/g, '').replace(/\'/g, '');
        const htmlString = readFile(root(SRC_DIR + '/' + file.path + '/' + filename));
        let strippedString = htmlString.split('\n').map(function(line) { return line.trim(); }).join('').split('\b').map(function(line) { return line.trim(); }).join('');
        result = result.replace(templateUrl[0], 'template: `' + strippedString + '`');
    }

    if (result !== fileString) {
        writeFile(fullFileName, result);
    }
}

// Main loop
getJsFiles().map(file => {
    inlineStyles(file);
    inlineHtml(file);
});