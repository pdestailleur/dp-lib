var fs = require('fs');
var root = require('./helpers').root;

var src = root('package.json');
var dst = root('dist/package.json');
var file = require(src);

let version = file.version;

let semVer = version.split('.');

let part = -1;

process.argv.forEach(function(val, index, array) {
    if (val === '--major') {
        part = 0;
    } else if (val === '--minor') {
        part = 1;
    } else if (val === '--bugfix') {
        part = 2;
    }
});

if (part > -1) {
    semVer.forEach((versionpart, index, semVer) => {
        console.log(part, index, versionpart);
        if (part == index) {
            semVer[index] = Number(versionpart) + 1 + '';
        } else {
            if (index > part) {
                semVer[index] = 0;
            }
        }
    });
    console.log(semVer);
    file.version = semVer.join('.');

    writeFile(src, file);
}

file.name = file.name.replace('-src', '');
file.scripts = undefined;
file.devDependencies = undefined;
file.peerDependencies = file.dependencies;
file.dependencies = undefined;

writeFile(dst, file);

function writeFile(file, content) {
    fs.writeFile(file, JSON.stringify(content, null, 2), error => {
        if (error) {
            console.log(error)
        }
    });
}